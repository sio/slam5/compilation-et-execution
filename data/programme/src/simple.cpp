/* Programme simple pour la démonstration du
 * principe de compilation.
 *
 * Auteur : Grégory DAVID <gregory.david@ac-nantes.fr>
 */
#include "simple.h"

int diviseur = 2;  // Variable globale, elle est visible partout

int main()
{
  int valeur;  // Variable locale visible que dans la fonction main
  valeur = calculer();
  return valeur;
}

int calculer()
{
  int code_retour;  // Variable locale visible que dans la fonction calculer
  code_retour = 96;
  code_retour /= diviseur;
  code_retour -= code_retour / diviseur;
  code_retour *= diviseur;
  return code_retour;
}
