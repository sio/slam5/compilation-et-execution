#include <iostream>
#include <postgresql/libpq-fe.h>
#include "toolbox.h"

static std::size_t max_iteration;

void initialisation(std::size_t maximum)
{
  max_iteration = maximum;
}

void SuperFunction()
{
  for(std::size_t i = 0; i < max_iteration; ++i)
  {
    std::cout << "Le compteur vaut : " << i << ", et son quadruple : " << (i << 2) << std::endl;
  }

  std::cout << std::endl;
  std::cerr << "INFO: Ce programme utilise la version "
            << PQlibVersion()
            << " de la bibliothèque 'libpq'" << std::endl;
}
