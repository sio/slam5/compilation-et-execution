% Principes et mise en œuvre de la compilation et de l'exécution d'un
% programme
%
% Auteur : Gregory DAVID
%%

\documentclass[12pt,a4paper,oneside,titlepage,final]{exam}

\usepackage{style/layout} \newcommand{\PROGRAMDIR}{data/programme}
\usepackage[hide=true,dir=\PROGRAMDIR]{bashful}
\lstdefinestyle{bashfulScript}{basicstyle=\scriptsize\ttfamily,emptylines=1}
\lstdefinestyle{bashfulStdout}{basicstyle=\scriptsize\ttfamily,emptylines=1}
\lstset{basicstyle={\scriptsize\ttfamily},emptylines=1}

\usepackage[pdf]{graphviz}

\newcommand{\MONTITRE}{Compilation et Exécution}
\newcommand{\MONSOUSTITRE}{principes et mise en œuvre}
\newcommand{\DISCIPLINE}{\glsentrytext{SiQUATRE} -- \glsentrydesc{SiQUATRE}}

\usepackage[%
	pdftex,%
	pdfpagelabels=true,%
	pdftitle={\MONTITRE},%
	pdfauthor={Grégory DAVID},%
	pdfsubject={\MONSOUSTITRE},%
        colorlinks,%
]{hyperref}
\usepackage{style/glossaire}
\addto\captionsfrench{%
  \renewcommand*{\glossaryname}{Lexique}%
  \renewcommand*{\acronymname}{Liste des acronymes}%
}
\usepackage{style/commands}
\usepackage{tikz}
\usetikzlibrary{decorations.text} % Pour avoir le texte qui suit une courbe

\title{
  \begin{tabular*}{\linewidth}{@{\extracolsep{\fill}}lr}
    & {\Huge {\bf \MONTITRE}} \\
    & {\huge \MONSOUSTITRE} \\
    & {\large \DISCIPLINE} \\
  \end{tabular*}
}

%\printanswers\groolotPhiligranne{CORRECTION}

\longnewglossaryentry{preprocesseur}{name=pr{é}processeur,
  sort=preprocesseur, see={analyse lexicale,analyse syntaxique}}{En
  informatique, un préprocesseur est un programme qui procède à des
  transformations sur un \gls{code source}, avant l'étape de
  traduction proprement dite (\gls{compilation} ou
  interprétation). Les préprocesseurs sont des exemples typiques de
  langages spécialisés. Selon le niveau de leur analyse du texte
  source, on distingue des préprocesseurs lexicaux et des
  préprocesseurs syntaxiques.}

\longnewglossaryentry{analyse lexicale}{name=analyse lexicale,
  see=preprocesseur}{En informatique, l'analyse lexicale,
  \emph{lexing}, segmentation ou \emph{tokenization} (à différencier
  du terme tokenization utilisé en sécurité informatique) fait partie
  de la première phase de la \textbf{chaîne de compilation}. Elle
  consiste à convertir une chaîne de caractères en une liste de
  symboles (\emph{tokens} en anglais). Ces symboles sont ensuite
  consommés lors de l'\gls{analyse syntaxique}. Un programme réalisant
  une analyse lexicale est appelé un analyseur lexical,
  \emph{tokenizer} ou \emph{lexer}. Un analyseur lexical est
  généralement combiné à un analyseur syntaxique pour analyser la
  syntaxe d'un texte.}

\longnewglossaryentry{analyse syntaxique}{name=analyse syntaxique,
  see=preprocesseur}{ Consiste à mettre en évidence la structure d'un
  texte, généralement une phrase écrite dans une langue naturelle,
  mais on utilise également cette terminologie pour l'analyse d'un
  programme informatique. L'analyseur syntaxique (parser, en anglais)
  est le programme informatique qui réalise cette tâche. Cette
  opération suppose une formalisation du texte, qui est vu le plus
  souvent comme un élément d'un langage formel, défini par un ensemble
  de règles de syntaxe formant une grammaire formelle. La structure
  révélée par l'analyse donne alors précisément la façon dont les
  règles de syntaxe sont combinées dans le texte.}

\longnewglossaryentry{compilation}{name=compilation}{En informatique,
  c'est le terme utilisé pour désigner l'étape de la chaîne de
  compilation d'un programme qui transforme un \gls{code source} écrit
  dans un langage de programmation (le langage source) en un autre
  langage informatique (appelé langage cible). Pour qu'il puisse être
  exploité par l'assembleur, le compilateur traduit le code source,
  écrit dans un langage de haut niveau d'abstraction, facilement
  compréhensible par l'humain, vers un langage de plus bas niveau : un
  langage d'assemblage.

  La compilation est souvent suivie de l'étape d'\gls{assemblage} pour
  générer un \gls{fichier objet} (fichier avec le suffixe
  \texttt{.o}).}

\longnewglossaryentry{assemblage}{name=assemblage}{Un langage
  d'assemblage ou langage assembleur est, en programmation
  informatique, un langage de bas niveau qui représente le langage
  machine sous une forme lisible par un humain. Les combinaisons de
  \emph{bits} du langage machine sont représentées par des symboles
  dits « mnémoniques » (du grec \emph{mnêmonikos}, relatif à la
  mémoire), c'est-à-dire faciles à retenir. Le programme assembleur
  convertit ces mnémoniques en code machine, ainsi que les valeurs en
  binaire et les libellés d'emplacements en adresses, en vue de créer
  par exemple un \gls{fichier objet} ou un \gls{fichier executable}.

  Dans la pratique courante, le même terme assembleur est utilisé à la
  fois pour désigner le langage d'assemblage et le programme
  assembleur qui le traduit. On parle ainsi de « programmation en
  assembleur ».}

\longnewglossaryentry{edition des liens}{name={é}dition des liens,
  sort={edition des liens}, see={fichier executable,fichier
    objet}}{Lors du développement d'un programme informatique,
  l’édition des liens est un processus qui permet de créer des
  fichiers exécutables ou des bibliothèques dynamiques ou statiques, à
  partir de fichiers objets et de bibliothèques statiques.

  La compilation d'un fichier source vers un fichier objet laisse
  l'identification de certains symboles à plus tard. Avant de pouvoir
  exécuter ces fichiers objets, il faut résoudre les symboles et les
  lier à des bibliothèques ou d'autres fichiers objets. Le lien avec
  une bibliothèque peut être :

  \begin{description}
    \item[statique] : le fichier objet et la bibliothèque sont liés
    dans le même fichier exécutable ;
    \item[dynamique] : le fichier objet est lié avec la bibliothèque,
    mais pas dans le même fichier exécutable ; les liens dynamiques
    sont établis pendant l'exécution du fichier exécutable,
    c'est-à-dire du programme exécutable.
  \end{description}
  Les assembleurs et les compilateurs sont généralement livrés avec un
  lieur (\emph{linker}) ou \textbf{éditeur de liens}, un programme
  chargé de faire l'édition des liens.}

\longnewglossaryentry{fichier objet}{name=fichier objet, see={fichier
    executable}}{En programmation informatique, un fichier objet est
  un fichier intermédiaire intervenant dans le processus de
  compilation.

  Ce fichier contient du code machine, ainsi que d'autres informations
  :
  \begin{itemize}
    \item nécessaires à l'édition de liens (symboles) ;
    \item nécessaires lors de la phase de déboguage.
  \end{itemize}
  La plupart des compilateurs donnent à ces fichiers le suffixe
  \texttt{.o} (comme le \gls{GCC}) et d'autres \texttt{.obj} (comme le
  compilateur de Microsoft vendu dans le produit Visual C++).}

\longnewglossaryentry{fichier executable}{name=fichier exécutable,
  see={fichier objet,interpreteur}}{En informatique et en technologies
  de l'information, un fichier exécutable, parfois (par métonymie) un
  programme, ou simplement un exécutable est un fichier contenant un
  programme et identifié par le système d'exploitation en tant que
  tel. Le chargement d'un tel fichier entraîne la création d'un
  processus dans le système, et l'exécution du programme. Par
  opposition, le fichier de données doit d'abord être interprété par
  un programme (l'\gls{interpreteur}) pour prendre sens.}

\longnewglossaryentry{code source}{name={code source}, plural={codes
    source}}{Le code source (ou les sources, voire le source) est un
  ensemble d'instructions écrites dans un langage de programmation
  informatique de haut niveau, compréhensible par un être humain
  entraîné, permettant d'obtenir un programme pour un ordinateur.

  Les systèmes d'exploitation ne peuvent pas directement exploiter le
  code source ; ils ne peuvent que lancer des exécutables.}

\longnewglossaryentry{interpreteur}{name=interpr{é}teur,
  sort=interpreteur, see={code source}}{Un interprète, ou
  interpréteur, est un outil informatique ayant pour tâche d'analyser,
  de traduire et d'exécuter un programme écrit dans un langage
  informatique. De tels langages sont dits langages interprétés.

  L'interprète est capable de lire le code source d'un langage sous
  forme de script, habituellement un fichier texte, et d'en exécuter
  les instructions après une analyse syntaxique du
  contenu. Généralement ces langages textuels sont appelés des
  langages de programmation. Cette interprétation conduit à une
  exécution d'actions.}

\longnewglossaryentry{compilateur}{name=compilateur,
  see={compilation,code source,fichier objet,interpreteur}}{Un
  compilateur est un programme informatique qui traduit un langage, le
  langage du \gls{code source}, en un autre, appelé le langage cible
  (ou langage objet), en préservant la signification du texte
  source. Ce schéma général décrit un grand nombre de programmes
  différents ; et ce que l'on entend par \og~signification du texte
  source~\fg dépend du rôle du compilateur. Lorsque l'on parle de
  compilateur, on suppose aussi en général que le langage source est,
  pour l'application envisagée, de plus haut niveau que le langage
  cible, c'est-à-dire qu'il présente un niveau d'abstraction
  supérieur.

  En pratique, un compilateur sert le plus souvent à traduire un code
  source écrit dans un langage de programmation en un autre langage,
  habituellement un langage d'assemblage ou un code machine. Le
  programme en code machine produit par un compilateur est appelé code
  objet.

  Le premier compilateur, \texttt{A-0 System}, a été écrit en 1951 par
  Grace Hopper.}

\longnewglossaryentry{syntaxe ATnT}{name=syntaxe \glsentrytext{ATnT}}{
  La syntaxe \gls{ATnT} correspond à l'une des variantes du langage
  d'assemblage des processeurs \texttt{x86}. Il s'agit d'une
  transposition vers cette architecture du langage reconnu par
  l'assembleur de \gls{UNIX}, \texttt{as}, qui ciblait alors les
  mini-ordinateurs de la famille \texttt{PDP}.}

\newacronym{GCC}{GCC}{\gls{GNU} Compiler Collection}
\newacronym{ATnT}{AT\&T}{American Telephone \& Telegraph Company}

\begin{document}
% Page de titre
\maketitle

% Copyright
\include{copyright}

\newpage
\vspace*{\fill}
\tableofcontents{}
\vspace*{\fill}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}

\clearpage %
\label{sec:glossaire}
\printglossary[toctitle={\glossaryname}]

\label{sec:acronymes}
\printacronyms[toctitle={\acronymname}]

\clearpage %
\section{Principes}\label{sec:principes}
\subsection{Rappels}\label{sec:principes.rappels}
L'architecture logicielle d'un ordinateur repose sur les éléments
décrits dans la \figurename~\vref{fig:archi.logicielle}.

\begin{figure}[h!]
  \centering
  \begin{tikzpicture}[scale=0.75]
    \coordinate (center) at (0,0); %
    \filldraw[rotate=20, fill=yellow!1, draw=black] (center) -- (11,0)
    arc (0:120:11) -- cycle; % Applicatifs
    \path[rotate=20, decoration={text along path,
      text={|\bf|Applicatifs}, text align={align=center}, text
      color=black, reverse path}, decorate] (0:10) arc (0:120:10);

    \filldraw[rotate=100, fill=yellow!90!black!10, draw=black]
    (center) -- (10,0) arc (0:60:10) -- cycle; % Langage interprété
    \path[rotate=100, decoration={text along path, text={|\bf|Langages
        interpretes}, text align={align=center}, text color=black,
      reverse path}, decorate] (0:9.25) arc (0:60:9.25);
    \path[rotate=100, decoration={text along path, text={|\bf|(Python,
        Ruby, PHP, etc.)}, text align={align=center}, text
      color=black, reverse path}, decorate] (0:8.75) arc (0:60:8.75);

    \filldraw[rotate=10, fill=yellow!80!black!20, draw=black] (center)
    -- (8,0) arc (0:120:8) -- cycle; % Langage de haut niveau
    \path[rotate=10, decoration={text along path, text={|\bf|Langages
        de haut niveau (C/C++, Java, etc.)}, text
      align={align=center}, text color=black, reverse path}, decorate]
    (0:7) arc (0:120:7);

    \filldraw[rotate=80, fill=yellow!70!black!30, draw=black] (center)
    -- (6,0) arc (0:90:6) -- cycle; % Assembleur
    \path[rotate=80, decoration={text along path,
      text={|\bf|Assembleur}, text align={align=center}, text
      color=black, reverse path}, decorate] (0:5) arc (0:90:5);

    \filldraw[fill=yellow!60!black!40, draw=black] (center) -- (4,0)
    arc (0:120:4) -- cycle; %Système d'exploitation
    \path[decoration={text along path, text={|\bf|Operating System},
      text align={align=center}, text color=black, reverse path},
    decorate] (0:3) arc (0:120:3);

    \filldraw[fill=yellow!50!black!50, draw=black] (center) -- (2,0)
    arc (0:180:2) -- cycle; % Physique
    \draw (0,0.25) node[font=\bf]{Code machine};
  \end{tikzpicture}
  \caption{Architecture en pelure d'oignon}
  \label{fig:archi.logicielle}
\end{figure}

Alors, nous concevons nos outils en nous basant sur des couches
inférieures, en fonction du niveau auquel nous nous trouvons. Quoi
qu'il en soit, nous devons toujours nous reposer sur le cœur de notre
système : le code machine donnant accès au matériel. L'ensemble des
contraintes que nous rencontrerons seront bien souvent issues des
contraintes imposées par le matériel.

À titre d'exemples, nous pourrions rencontrer les contraintes
suivantes :
\begin{itemize}
  \item la quantité de mémoire disponible,
  \item le nombre de fichiers que l'on peut écrire sur un support,
  indépendamment de leurs tailles,
  \item la profondeur de couleur d'un écran,
  \item la vitesse d'écriture d'un paquet sur l'interface réseau,
  \item \emph{etc}.
\end{itemize}

\clearpage %
\subsection{Les phases de traitement}\label{sec:principes.phases}
Afin de passer du \gls{code source} à un \gls{fichier
  executable}~\cite{wiki:FICHIER_EXECUTABLE}, nous devons effectuer
quatre phases de traitement, décrites ci-après et présentées dans le
schéma de la \figurename~\vref{fig:chaine.compilation.generale}.

\begin{enumerate}
  \item \Gls{preprocesseur} ;
  \item \Gls{compilation} ;
  \item \Gls{assemblage} ;
  \item \Gls{edition des liens}.
\end{enumerate}

\begin{figure}[h]
  \centering
  \digraph[height=10cm]{chaineCompilationGenerale}{
    graph [splines=yes,concentrate=yes,center=yes,fontname=Courier];
    edge [fontsize=14,fontname=Courier];
    node [fontname=Courier,shape=note];
    cpp[label=".cpp"];
    h[label=".h"];
    ii[label=".ii"];
    s[label=".s"];
    o[label=".o"];
    executable[label="executable"];
    subgraph cluster_legend{
      graph[label="Légende",labeljust="l"];
      legendelibh[label="<lib.h>"];
      legendefichiercpp[label="fichier.cpp"];
      legendefichiero[label="fichier.o"];
      legendelib[label="lib.so"];
      legendeexecutable[label="executable"];
      legendelibh -> legendefichiercpp[style="dotted", label="\#include"];
      legendefichiercpp -> legendefichiero[label="conversion"];
      legendefichiero -> legendeexecutable;
      legendelib -> legendeexecutable[style="dashed", label="lien"];
    };
    subgraph cluster_stdcpp{
      graph[label="GNU C++ Library"];
      libstdcpp[label="libstdc++.so"];
    };
    h -> cpp[style="dotted", label="\#include"];
    cpp -> ii[label="Préprocesseur (g++ -E)"];
    ii -> s[label="Compilation (g++ -S)"];
    s -> o[label="Assemblage (g++ -c)"];
    o -> executable[label="Édition des liens (g++)"];
    libstdcpp -> executable[style="dashed"];
  }
  \caption{Chaîne de compilation générale schématisée. Entre
    parenthèses, vous trouvez l'option \texttt{g++} à utiliser pour
    assurer la conversion.}\label{fig:chaine.compilation.generale}
\end{figure}

\clearpage %
\section{Mise en œuvre simple}\label{sec:mise.en.oeuvre.simple}
La \figurename~\vref{fig:chaine.compilation.simple} expose une
schématisation simple d'une chaîne de compilation d'un fichier écrit
en \texttt{C++}.

Voir la \figurename~\vref{fig:chaine.compilation.elaboree} pour une
version élaborée d'un programme et de sa chaîne de compilation.

\begin{figure}[p]
  \centering
  \digraph[height=10cm]{chaineCompilationSimple}{
    graph [splines=yes,concentrate=yes,center=yes,fontname=Courier];
    edge [fontsize=14,fontname=Courier];
    node [fontname=Courier,shape=note];
    cpp[label="src/simple.cpp"];
    h[label="src/simple.h"];
    ii[label="src/simple.ii"];
    s[label="src/simple.s"];
    o[label="obj/simple.o"];
    subgraph cluster_legend{
      graph[label="Légende",labeljust="l"];
      legendelibh[label="<lib.h>"];
      legendefichiercpp[label="fichier.cpp"];
      legendefichiero[label="fichier.o"];
      legendelib[label="lib.so"];
      legendeexecutable[label="executable"];
      legendelibh -> legendefichiercpp[style="dotted", label="\#include"];
      legendefichiercpp -> legendefichiero[label="conversion"];
      legendefichiero -> legendeexecutable;
      legendelib -> legendeexecutable[style="dashed", label="lien"];
    };
    subgraph cluster_stdcpp{
      graph[label="GNU C++ Library"];
      libstdcpp[label="libstdc++.so"];
    };
    executable[label="bin/executable.simple"];
    h -> cpp[style="dotted", label="\#include"];
    cpp -> ii[label="pre-processing (g++ -E)"];
    ii -> s[label="Compilation (g++ -S)"];
    s -> o[label="Assemblage (g++ -c)"];
    o -> executable[label="Édition des liens (g++)"];
    libstdcpp -> executable[style="dashed"];
  }
  \caption{Chaîne de compilation schématisée, pour un
    \textbf{programme simple}. L'ensemble de la chaîne de compilation
    est assurée par \texttt{g++}, et les programmes d'assemblage
    \texttt{as} et d'édition des liens \texttt{ld} sont
    automatiquement appelés par
    \texttt{g++}.}\label{fig:chaine.compilation.simple}
\end{figure}



\subsection{Structure initiale du système de
  fichiers}\label{sec:structure.initiale.du.systeme.de.fichiers.simple}

Le \lstlistingname~\vref{lst:structure.simple} présente l'état initial
du système de fichiers pour l'ensemble du projet. Les répertoires
\texttt{bin}, \texttt{obj} et \texttt{src} sont créés par le
développeur afin d'assurer une meilleure répartition des \glspl{code
  source} et des fichiers résultats de la compilation.

\lstset{caption={Structure initiale du projet de développement},
  label={lst:structure.simple}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
rm bin/*
rm obj/*.o
rm src/*.ii
rm src/*.s
tree -i -n -f -l -F -P "*simple*" -I "*\~|*.orig"
\END

\subsection[pre-processing]{\emph{Pre-processing} :
  \gls{preprocesseur}}\label{sec:pre.processing.simple}
Considérons le fichier et le \gls{code source} d'un programme simple,
présenté en
\lstlistingname~\vrefrange{lst:structure.simple}{lst:simple.h}.

\paragraph{Fichiers d'entrée}~ %
Pour cette première étape, les fichiers des
\lstlistingname~\vrefrange{lst:simple.cpp}{lst:simple.h} sont utilisés
pour assurer l'\gls{analyse syntaxique} et l'\gls{analyse lexicale}.

\lstinputlisting[language={[11]C++}, caption={\lstname},
name={\texttt{src/simple.cpp}},
label={lst:simple.cpp}]{\PROGRAMDIR/src/simple.cpp}

\lstinputlisting[language={[11]C++}, caption={\lstname},
name={\texttt{src/simple.h}},
label={lst:simple.h}]{\PROGRAMDIR/src/simple.h}

\paragraph{Exécution du \gls{preprocesseur}}~ %
\lstset{language={bash}, caption={Phase du \gls{preprocesseur}},
  label={lst:simple.pp}}

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Preprocesseur
g++ -E src/simple.cpp -o src/simple.ii
\END

\paragraph{Fichier de sortie} %
Nous pouvons remarquer, dans le fichier issu du \gls{preprocesseur}
(voir \lstlistingname~\vref{lst:simple.ii}), que les commentaires ont
été enlevés, que le contenu du fichier \texttt{src/simple.h} a été
inclus et que d'autres informations ont été ajoutées. Ce fichier sera
celui qui sera utilisé pour assurer la phase de
compilation. \textbf{Le langage de ce fichier reste le C++}.

\lstset{language={[11]C++}, caption={\lstname}}%
\lstinputlisting[name={\texttt{src/simple.ii}},
label={lst:simple.ii}]{\PROGRAMDIR/src/simple.ii}



\clearpage %
\subsection{Compilation}\label{sec:compilation.simple}
\paragraph{Fichier d'entrée}~ %
Voir le \lstlistingname~\vref{lst:simple.ii}.

\paragraph{Exécution de la \gls{compilation}}~ %
\lstset{language={bash}, caption={Phase de \gls{compilation}},
  label={lst:simple.cc}}

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Compilation
g++ -S src/simple.ii -o src/simple.s
\END

\paragraph{Fichier de sortie} %
Nous pouvons remarquer, dans le fichier issu de la \gls{compilation}
(voir \lstlistingname~\vref{lst:simple.s}), que le \gls{code source}
en \texttt{C++} a été traduit dans le langage assembleur de bas
niveau. Ici, c'est le langage d'assemblage \texttt{GNU AS} syntaxe
\texttt{AT\&T}~\cite{wikibooks:x86ASM,wikibooks:x86GAS,ASM_SF_NET}
pour le système \gls{GNULINUX} et un microprocesseur \texttt{x86\_64}.

\lstset{language=[x86masm]Assembler, caption={\lstname}}%
\lstinputlisting[name={\texttt{src/simple.s}},
label={lst:simple.s}]{\PROGRAMDIR/src/simple.s}



\clearpage %
\subsection{Assemblage}\label{sec:assemblage.simple}
\paragraph{Fichier d'entrée}~ %
Voir le \lstlistingname~\vref{lst:simple.s}.

\paragraph{Exécution de l'\gls{assemblage}}~ %
\lstset{language={bash}, caption={Phase d'\gls{assemblage}},
  label={lst:simple.gas}}

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Assemblage
g++ -c src/simple.s -o obj/simple.o
\END

\paragraph{Fichier de sortie} %
Nous pouvons remarquer, dans le fichier issu de l'\gls{assemblage}
(voir \lstlistingname~\vref{lst:simple.o}), que le \gls{code source}
en langage d'assembleur de bas niveau a produit du \textbf{code
  machine}, ici rendu visible par l'utilitaire \texttt{hexdump}.

\lstset{language={}, caption={Code machine du fichier
    \texttt{obj/simple.o} rendu visible par \texttt{hexdump -C}},
  label={lst:simple.o}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
hexdump -C obj/simple.o
\END

\paragraph{Table des symboles} %
L'utilitaire \texttt{nm} nous permet d'afficher la liste des symboles
contenus dans un fichier binaire. En lui passant en paramètre notre
fichier objet \texttt{obj/simple.o}, nous retrouvons notamment le nom
de notre fonction principale \texttt{main}, le nom de notre fonction
de calcul \texttt{calculer} et le nom de la variable globale
\texttt{diviseur}. Voir le \lstlistingname~\vref{lst:simple.o.syms}.

\lstset{language={}, caption={Table des symboles du fichier
    \texttt{obj/simple.o} rendue visible par \texttt{nm}},
  label={lst:simple.o.syms}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
nm obj/simple.o
\END

\subsubsection{Désassemblage}\label{sec:desassemblage.simple}
Nous pouvons nous intéresser aussi au procédé inverse à l'assemblage :
le désassemblage. Celui-ci permet, depuis un fichier objet --
\texttt{obj/simple.o} dans notre cas --, d'interpréter le code machine
et d'en afficher une possible écriture en langage d'assembleur de bas
niveau.

L'utilitaire \texttt{objdump} permet entre autre de réaliser cela en
lui fournissant l'option \texttt{-{-}disassemble}. Le
\lstlistingname~\vref{lst:simple.o.s} montre le désassemblage de notre
fichier \texttt{obj/simple.o}.

Ceci est à comparer avec le code produit par le compilateur dans le
\lstlistingname~\vref{lst:simple.s}.

\lstset{language={[x86masm]Assembler}, caption={Désassemblage du
    fichier \texttt{obj/simple.o} rendu visible par \texttt{objdump
      {-}{-}disassemble}}, label={lst:simple.o.s}}%

\bash[ignoreExitCode=true,ignoreStderr=true,prefix={}]
LANG='' objdump -d obj/simple.o
\END
\lstinputlisting{\PROGRAMDIR/\jobname.stdout}


\clearpage %
\subsection{Édition des liens}\label{sec:edition.des.liens.simple}
\paragraph{Fichier d'entrée}~ %
Voir le \lstlistingname~\vref{lst:simple.o}.

\paragraph{Exécution de l'\gls{edition des liens}}~ %
\lstset{language={bash}, caption={Phase d'\gls{edition des liens}},
  label={lst:simple.linker}}

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Edition des liens
g++ obj/simple.o -o bin/executable.simple
\END


\paragraph{Fichier de sortie} %
Nous pouvons remarquer, dans le fichier issu de l'\gls{edition des
  liens}, que le code machine a été \textbf{lié} avec d'autres
éléments lui conférant alors sa qualité d'exécutable. Le code machine
final peut être rendu visible par l'utilitaire \texttt{hexdump}, mais
est trop long pour être affiché ici.

\paragraph{Table des symboles} %
Voici cependant, dans le \lstlistingname~\vref{lst:simple.syms}, la
liste des symboles du fichier \texttt{bin/simple}, où nous retrouvons
nos trois symboles \texttt{main}, \texttt{calculer} et
\texttt{diviseur} tel que déjà vus dans le
\lstlistingname~\vref{lst:simple.syms} ainsi que d'autres symboles
issus des bibliothèques liées.

\lstset{language={sh}, caption={Table des symboles du fichier
    \texttt{bin/simple} rendue visible par \texttt{nm}},
  label={lst:simple.syms}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
nm bin/executable.simple
\END

\paragraph{Bibliothèques liées}
Voici, dans le \lstlistingname~\vref{lst:simple.ldd}, la liste des
bibliothèques liées au fichier \texttt{bin/simple}, où on y retrouve
notamment la bibliothèque standard C++ \texttt{libstdc++.so.6}.

\lstset{language={sh}, caption={Liste des bibliothèques liées à
    l'exécutable \texttt{bin/simple} rendue visible par \texttt{ldd}},
  label={lst:simple.ldd}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
ldd bin/executable.simple
\END



\subsection{Exécution du
  programme}\label{sec:execution.du.programme.simple}
Lorsque nous avons (enfin) obtenu notre exécutable, il ne nous reste
plus qu'à l'utiliser en l'exécutant.

\paragraph{Exécution de l'exécutable}~ %
\lstset{language={bash}, caption={Appel/Invocation du programme
    exécutable}, label={lst:simple.call}}

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Invocation
bin/executable.simple
\END

\paragraph{Sorties du programme} Le programme ne réalise aucune sortie
sur la sortie standard \texttt{stdout} ni sur la sortie standard des
erreurs \texttt{stderr}.

En revanche, le code de sortie du programme est \texttt{48}. Pourquoi
?

\subsection{Structure finale du système de
  fichiers}\label{sec:structure.finale.du.systeme.de.fichiers.simple}
\lstset{caption={Structure finale du projet de développement},
  label={lst:structure.simple.finale}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
tree -i -n -f -l -F -P "*simple*" -I "*\~|*.orig"
\END


\clearpage%
\section{Mise en œuvre plus élaborée}
\begin{figure}[p]
  \centering
  \digraph[width=16cm]{chaineCompilationElaboree}{
    graph [splines=yes,concentrate=yes,center=yes,fontname=Courier];
    edge [fontsize=14,fontname=Courier];
    node [fontname=Courier,shape=note];
    elaborecpp[label="elabore.cpp"];
    elaboreii[label="elabore.ii"];
    elabores[label="elabore.s"];
    elaboreo[label="elabore.o"];
    toolcpp[label="toolbox.cpp"];
    toolh[label="toolbox.h"];
    toolii[label="toolbox.ii"];
    tools[label="toolbox.s"];
    toolo[label="toolbox.o"];
    subgraph cluster_legend{
      graph[label="Légende",labeljust="l"];
      legendelibh[label="<lib.h>"];
      legendefichiercpp[label="fichier.cpp"];
      legendefichiero[label="fichier.o"];
      legendelib[label="lib.so"];
      legendeexecutable[label="executable"];
      legendelibh -> legendefichiercpp[style="dotted", label="\#include"];
      legendefichiercpp -> legendefichiero[label="conversion"];
      legendefichiero -> legendeexecutable;
      legendelib -> legendeexecutable[style="dashed", label="lien"];
    };
    subgraph cluster_stdcpp{
      graph[label="GNU C++ Library"];
      libstdcpp[label="libstdc++.so"];
      iostream[label="<iostream>"];
      cstddef[label="<cstddef>"];
      argph[label="<argp.h>"];
    };
    subgraph cluster_libpq{
      graph[label="PostgreSQL library"];
      libpq[label="libpq.so"];
      libpqh[label="postgresql/libpq-fe.h"];
    };
    executable[label="executable.elabore"];
    {argph toolh} -> elaborecpp[style="dotted"];
    elaborecpp -> elaboreii -> elabores -> elaboreo;
    cstddef -> toolh[style="dotted"];
    iostream -> toolcpp[style="dotted"];
    {toolh libpqh} -> toolcpp[style="dotted"];
    toolcpp -> toolii -> tools -> toolo;
    {libstdcpp libpq} -> executable[style="dashed"];
    {elaboreo toolo} -> executable;
  }
  \caption{Chaîne de compilation schématisée, pour un
    \textbf{programme élaboré}. L'ensemble de la chaîne de compilation
    est assurée par
    \texttt{g++}.}\label{fig:chaine.compilation.elaboree}
\end{figure}


\subsection{Structure initiale du système de
  fichiers}\label{sec:structure.initiale.du.systeme.de.fichiers.elabore}
Le \lstlistingname~\vref{lst:structure.elabore} présente l'état
initial du système de fichiers pour l'ensemble du projet. Les
répertoires \texttt{bin}, \texttt{obj} et \texttt{src} sont créés par
le développeur afin d'assurer une meilleure répartition des
\glspl{code source} et des fichiers résultats de la compilation.

\lstset{caption={Structure initiale du projet de développement},
  label={lst:structure.elabore}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
rm bin/*
rm obj/*.o
rm src/*.ii
rm src/*.s
tree -i -n -f -l -F -P "*elabore*|toolbox*" -I "*\~|*.orig"
\END

\subsection[pre-processing]{\emph{Pre-processing} :
  \gls{preprocesseur}}\label{sec:pre.processing.elabore}
Considérons les fichiers et le \gls{code source} d'un programme un peu
plus élaboré, présenté en
\lstlistingname~\vrefrange{lst:structure.elabore}{lst:toolbox.cpp}.

\paragraph{Fichiers d'entrée}~%
\lstinputlisting[language={[11]C++}, caption={\lstname},
name={\texttt{src/elabore.cpp}},
label={lst:elabore.cpp}]{\PROGRAMDIR/src/elabore.cpp}

\lstinputlisting[language={[11]C++}, caption={\lstname},
name={\texttt{src/toolbox.h}},
label={lst:toolbox.h}]{\PROGRAMDIR/src/toolbox.h}

\lstinputlisting[language={[11]C++}, caption={\lstname},
name={\texttt{src/toolbox.cpp}},
label={lst:toolbox.cpp}]{\PROGRAMDIR/src/toolbox.cpp}

\paragraph{Exécution du \gls{preprocesseur}}~ %
\lstset{language={bash}, caption={Phase du \gls{preprocesseur}},
  label={lst:elabore.pp}}

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Preprocesseur
g++ -E src/elabore.cpp -o src/elabore.ii
g++ -E src/toolbox.cpp -o src/toolbox.ii
\END

\paragraph{Fichiers de sortie}~%
Les fichiers ainsi produit sont extrêmement long et ne sauraient être
intégrés à ce document.

\bash[prefix={}]
wc -l src/elabore.ii src/toolbox.ii
\END
\lstinputlisting[language={sh}, caption={Commande pour effectuer le
  comptage du nombre de lignes pour l'ensemble des fichiers
  \texttt{src/elabore.ii} et \texttt{src/toolbox.ii}},
label={lst:elabore.wc.ii.call}]{\PROGRAMDIR/\jobname.sh}
\lstinputlisting[language={sh}, caption={Affichage du nombre de lignes
  d'instructions suite au \gls{preprocesseur}},
label={lst:elabore.wc.ii.call.stdout}]{\PROGRAMDIR/\jobname.stdout}


\clearpage %
\subsection{Compilation}\label{sec:compilation.elabore}
\paragraph{Fichiers d'entrée}~ %
Voir les fichiers \texttt{src/elabore.ii} et \texttt{src/toolbox.ii}.

\paragraph{Exécution de la \gls{compilation}}~ %
\lstset{language={bash}, caption={Phase de \gls{compilation}},
  label={lst:elabore.cc}}

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Compilation
g++ -S src/elabore.ii -o src/elabore.s
g++ -S src/toolbox.ii -o src/toolbox.s
\END

\paragraph{Fichiers de sortie}~ %
\lstset{language=[x86masm]Assembler, caption={\lstname}}%
\lstinputlisting[lastline=20, name={\texttt{src/elabore.s}, extrait
  des 20 premières lignes},
label={lst:elabore.s}]{\PROGRAMDIR/src/elabore.s}

\lstinputlisting[lastline=20, name={\texttt{src/toolbox.s}, extrait
  des 20 premières lignes},
label={lst:toolbox.s}]{\PROGRAMDIR/src/toolbox.s}



\clearpage %
\subsection{Assemblage}\label{sec:assemblage.elabore}
\paragraph{Fichiers d'entrée}~ %
Voir les \lstlistingname~\vrefrange{lst:elabore.s}{lst:toolbox.s}.

\paragraph{Exécution de l'\gls{assemblage}}~ %
\lstset{language={bash}, caption={Phase d'\gls{assemblage}},
  label={lst:elabore.gas}}

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Assemblage
g++ -c src/elabore.s -o obj/elabore.o
g++ -c src/toolbox.s -o obj/toolbox.o
\END

\paragraph{Fichiers de sortie}~ %
Les fichiers \texttt{obj/elabore.o} et \texttt{obj/toolbox.o} sont de
taille conséquente et leur affichage n'apporte pas beaucoup
d'informations pertinente dans notre étude. Cependant, vous pouvez en
faire leur affichage à l'aide des commandes présentées dans le
\lstlistingname~\vref{lst:elabore.et.toolbox.o}.

\lstset{language={}, caption={Commandes d'affichage du code machine
    des fichiers \texttt{obj/elabore.o} et \texttt{obj/toolbox.o}
    rendu visible grâce à \texttt{hexdump -C}},
  label={lst:elabore.et.toolbox.o}}%

\bash[script,ignoreExitCode=true,ignoreStderr=true,prefix={}]
hexdump -C obj/elabore.o
hexdump -C obj/toolbox.o
\END

\paragraph{Table des symboles}~ %
\lstset{language={}, caption={Table des symboles du fichier
    \texttt{obj/elabore.o} rendue visible par \texttt{nm}},
  label={lst:elabore.o.syms}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
nm obj/elabore.o
\END

\lstset{language={}, caption={Table des symboles du fichier
    \texttt{obj/toolbox.o} rendue visible par \texttt{nm}},
  label={lst:toolbox.o.syms}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
nm obj/toolbox.o
\END

\subsubsection{Désassemblage}\label{sec:desassemblage.elabore}
Ceux-ci sont à comparer avec le code produit par le compilateur dans
les \lstlistingname~\vrefrange{lst:elabore.s}{lst:toolbox.s}.

\lstset{language={[x86masm]Assembler}, caption={Désassemblage du
    fichier \texttt{obj/elabore.o} rendu visible par \texttt{objdump
      {-}{-}disassemble}, limité aux 20 premières lignes},
  label={lst:elabore.o.s}}%

\bash[ignoreExitCode=true,ignoreStderr=true,prefix={}]
LANG="" objdump -d obj/elabore.o
\END
\lstinputlisting[lastline=20]{\PROGRAMDIR/\jobname.stdout}

\lstset{language={[x86masm]Assembler}, caption={Désassemblage du
    fichier \texttt{obj/toolbox.o} rendu visible par \texttt{objdump
      {-}{-}disassemble}, limité aux 20 premières lignes},
  label={lst:toolbox.o.s}}%

\bash[ignoreExitCode=true,ignoreStderr=true,prefix={}]
LANG="" objdump -d obj/toolbox.o
\END
\lstinputlisting[lastline=20]{\PROGRAMDIR/\jobname.stdout}


\clearpage %
\subsection{Édition des liens}\label{sec:edition.des.liens.elabore}
\paragraph{Fichiers d'entrée}~ %
Voir les fichiers \texttt{obj/elabore.o} et \texttt{obj/toolbox.o}.

\paragraph{Exécution de l'\gls{edition des liens}}~ %
\begin{lstlisting}[language={bash}, caption={Phase d'\gls{edition des
liens}}, label={lst:elabore.linker}]
# Édition des liens
g++ obj/elabore.o obj/toolbox.o -lpq -o bin/executable.elabore
\end{lstlisting}

L'option de la ligne de commande \texttt{-lpq} est un paramètre pour
l'éditeur de liens pour lui spécifier que l'on désire établir un lien
avec la bibliothèque partagée \texttt{libpq}, la bibliothèque de
PostgreSQL. Sans cette précision (voir
\lstlistingname~\vref{lst:elabore.linker.fail.call}), l'\gls{edition
  des liens} échouerai de la manière présentée en
\lstlistingname~\vref{lst:elabore.linker.fail.stderr}.

\begin{lstlisting}[language={bash}, caption={\Gls{edition des liens}
incorrectement paramétrée}, label={lst:elabore.linker.fail.call}]
# Édition des liens incorrecte du fait
# du manque d'informations sur le lien
# externe vers la 'libpq', or il est fait usage
# de la fonction 'PQlibVersion()' dans 'src/toolbox.cpp'
g++ obj/elabore.o obj/toolbox.o -o bin/executable.elabore
\end{lstlisting}

\bash[ignoreExitCode=true, ignoreStderr=true, prefix={}]
LANG='' g++ obj/elabore.o obj/toolbox.o -o bin/executable.elabore
\END

\lstinputlisting[language={bash}, caption={Échec de l'\Gls{edition des
    liens}},
label={lst:elabore.linker.fail.stderr}]{\PROGRAMDIR/\jobname.stderr}

% Refaire l'edition des liens correctement pour la suite de la
% construction
\bash
g++ obj/elabore.o obj/toolbox.o -lpq -o bin/executable.elabore
\END


\paragraph{Table des symboles} ~%
\lstset{language={sh}, caption={Table des symboles du fichier
    \texttt{bin/executable.elabore} rendue visible par \texttt{nm}},
  label={lst:elabore.syms}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
nm bin/executable.elabore
\END

Nous pouvons remarquer que l'ensemble des symboles, issus de
\texttt{obj/elabore.o} et \texttt{obj/toolbox.o}, sont disponibles
dans la table générale des symboles de l'exécutable
\texttt{executable.elabore}.

\paragraph{Bibliothèques liées}
Voici, dans le \lstlistingname~\vref{lst:elabore.ldd}, la liste des
bibliothèques liées au fichier \texttt{bin/executable.elabore}, où on
y retrouve notamment la bibliothèque standard C++
\texttt{libstdc++.so.6} et la bibliothèque de PostgreSQL
\texttt{libpq.so.5}.

\lstset{language={sh}, caption={Liste des bibliothèques liées à
    l'exécutable \texttt{bin/executable.elabore} rendue visible par
    \texttt{ldd}}, label={lst:elabore.ldd}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
ldd bin/executable.elabore
\END


\clearpage %
\subsection{Exécution du
  programme}\label{sec:execution.du.programme.elabore}
Lorsque nous avons (enfin) obtenu notre exécutable, il ne nous reste
plus qu'à l'utiliser en l'exécutant.

\paragraph{Exécution de l'exécutable}~ %

\bash[ignoreExitCode=true,ignoreStderr=true,prefix={}]
# Invocation
bin/executable.elabore
\END

\lstinputlisting[language={sh}, caption={Appel/Invocation du
  programme}, label={lst:elabore.call}]{\PROGRAMDIR/\jobname.sh}

\paragraph{Sorties du programme}~ %
\lstinputlisting[language={sh}, caption={Sortie standard de
  l'exécution du programme \texttt{./bin/executable.elabore}},
label={lst:elabore.call.stdout}]{\PROGRAMDIR/\jobname.stdout}

\lstinputlisting[language={sh}, caption={Sortie standard des erreurs
  de l'exécution du programme \texttt{./bin/executable.elabore}},
label={lst:elabore.call.stderr}]{\PROGRAMDIR/\jobname.stderr}


\subsection{Structure finale du système de
  fichiers}\label{sec:structure.finale.du.systeme.de.fichiers.elabore}
\lstset{caption={Structure finale du projet de développement},
  label={lst:structure.elabore.finale}}%

\bash[stdout,ignoreExitCode=true,ignoreStderr=true,prefix={}]
tree -i -n -f -l -F -P "*elabore*|toolbox*" -I "*\~|*.orig"
\END


% References bibliographiques
\clearpage%
\printbibheading%
\addcontentsline{toc}{section}{Références
  externes}\label{sec:references}
%\printbibliography[nottype=online,check=notonline,heading=subbibliography,title={Bibliographiques}]
\printbibliography[check=online,heading=subbibliography,title={Webographiques}]
\nocite{wiki:CODE_SOURCE, wiki:A0SYS, wiki:FICHIER_BINAIRE,
  wiki:FICHIER_EXECUTABLE, wiki:BIBLIOTHEQUE_LOGICIELLE,
  wikibooks:x86ASM, wikibooks:x86GAS, ASM_SF_NET}

% Nettoyage des fichiers de compilation et cie.
\bash[ignoreExitCode=true,ignoreStderr=true]
rm bin/*
rm obj/*
rm src/*.ii src/*.s src/*.orig
\END
\end{document}
